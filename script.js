// ==UserScript==
// @name         Bye bye Paywallzz
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Remove paywalls
// @author       SergioTaquine
// @match        https://www.marmiton.org/*
// @match        https://www.750g.com/*
// @match        https://www.allocine.fr/*
// @icon         https://www.google.com/s2/favicons?domain=marmiton.org
// @downloadURL  https://gitlab.com/sergiotaquine/bypass-paywalls/-/blob/master/script.js
// @updateURL    https://gitlab.com/sergiotaquine/bypass-paywalls/-/blob/master/script.js

// ==/UserScript==

(function() {
    'use strict';

    setTimeout(() => {
        document.getElementsByClassName('didomi-popup-open')[0].setAttribute('style', 'overflow: auto !important');
        var leftColumn = document.getElementById('didomi-host');
        leftColumn.remove();
    }, 1500)
})();
